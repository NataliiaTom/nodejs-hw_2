const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const {authenticateJWT} = require('./middlewares/auth');

const {
  getNotes,
  AddNotes,
  getNote,
  updateNote,
  checkNote,
  deleteNote,
} = require('../controllers/notesController');

router.get('/', authenticateJWT, getNotes);
router.post('/', authenticateJWT, AddNotes);
router.get('/:id', authenticateJWT, getNote);
router.put('/:id', authenticateJWT, updateNote);
router.patch('/:id', authenticateJWT, checkNote);
router.delete('/:id', authenticateJWT, deleteNote);

module.exports = router;
