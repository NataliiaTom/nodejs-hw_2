const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const {authenticateJWT} = require('./middlewares/auth');

const {
  profileInfo,
  deleteProfileInfo,
  updateProfilePassword,
} = require('../controllers/userController');

router.get('/me', authenticateJWT, profileInfo);
router.delete('/me', authenticateJWT, deleteProfileInfo);
router.patch('/me', authenticateJWT, updateProfilePassword);

module.exports = router;
