const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const {
  authRegistration,
  loginUser,
} = require('../controllers/authController');
const {validatRegistration} = require('./middlewares/validationMiddleware');

router.post('/register', validatRegistration, authRegistration);
router.post('/login', loginUser);

module.exports = router;
