const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');

module.exports.authenticateJWT = (req, res, next) => {
  const authorization = req.headers.authorization;
  if (authorization) {
    const [tokenType, tokenValue] = authorization.split(' ');
    if (tokenType === 'JWT' && tokenValue) {
      jwt.verify(tokenValue, JWT_SECRET, (err, user) => {
        if (err) {
          return res.sendStatus(403);
        }
        req.user = user;
        next();
      });
    } else {
      res.sendStatus(401);
    }
  } else {
    res.sendStatus(401);
  }
};
