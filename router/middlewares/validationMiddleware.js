const Joi = require('joi');

module.exports.validatRegistration = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string().min(2).max(30).required(),

    password: Joi.string().min(2).max(30).required(),
  });

  const {error} = schema.validate(req.body);


  if (error) {
    res.status(400).json({message: error.details[0].message});
  } else {
    next();
  }
};
