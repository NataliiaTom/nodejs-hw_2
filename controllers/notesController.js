const {Auth} = require('../models/authModel');
const {Note} = require('../models/notesModel');

module.exports.getNotes = async (req, res, next) => {
  const user = req.user;
  const {limit, offset} = req.query;
  const lim = limit ? parseInt(limit) : 10;
  const off = offset ? parseInt(offset) : 0;
  const savedNotes = await Note.find({userId: user._id})
      .select('-__v')
      .skip(off)
      .limit(lim);
  try {
    if (savedNotes) {
      res.status(200).json({
        notes: savedNotes,
      });
    } else {
      res.status(400).json({message: 'There are no notes yet'});
    }
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
};
module.exports.AddNotes = async (req, res, next) => {
  const user = req.user;
  const {text} = req.body;
  try {
    const foundUser = await Auth.findOne({_id: user._id});
    if (foundUser) {
      const newNote = await new Note({
        userId: foundUser._id,
        completed: false,
        text: text,
      });
      await newNote.save();
      res.status(200).json({message: 'Success'});
    } else {
      res.status(400).json({message: 'User does not exist'});
    }
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
};
module.exports.getNote = async (req, res, next) => {
  try {
    const id = req.params.id;
    const requestedNote = await Note.findOne({_id: id}).select('-__v');
    if (!requestedNote) {
      res.status(400).json({message: 'Wrong id indicated'});
    }
    res.status(200).json({note: requestedNote});
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
};
module.exports.updateNote = async (req, res, next) => {
  try {
    const id = req.params.id;
    const {text} = req.body;
    const requestedNote = await Note.findOne({_id: id}).select('-__v');
    if (!requestedNote) {
      res.status(400).json({message: 'Wrong id indicated'});
    }
    requestedNote.text = text;
    requestedNote.save();
    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
};
module.exports.checkNote = async (req, res, next) => {
  try {
    const id = req.params.id;
    const requestedNote = await Note.findOne({_id: id}).select('-__v');
    if (!requestedNote) {
      res.status(400).json({message: 'Wrong id indicated'});
    }
    requestedNote.completed = !requestedNote.completed;
    requestedNote.save();
    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
};
module.exports.deleteNote = async (req, res, next) => {
  try {
    const id = req.params.id;
    const requestedNote = await Note.findOne({_id: id}).select('-__v');
    if (!requestedNote) {
      res.status(400).json({message: 'Wrong id indicated'});
    }
    requestedNote.deleteOne({});
    requestedNote.save();
    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
};
