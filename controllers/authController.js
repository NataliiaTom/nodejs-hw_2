const {Auth} = require('../models/authModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {JWT_SECRET} = require('../router/config');

module.exports.authRegistration = async (req, res, next) => {
  try {
    const {username, password} = req.body;
    const pass = await bcrypt.hash(password, 10);
    const user = await new Auth({
      username,
      password: pass,
    });


    await user.save();
    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
};
module.exports.loginUser = async (req, res, next) => {
  try {
    const {username, password} = req.body;


    const user = await Auth.findOne({username});

    if (!user) {
      return res.status(400)
          .json({message: `There is no user with such ${username}`});
    }

    if (!(await bcrypt.compare(password, user.password))) {
      return res.status(400).json({message: `Wrong password`});
    }

    const token = jwt.sign(
        {username: user.username, _id: user._id},
        JWT_SECRET,
    );

    res.json({message: 'success', jwt_token: token});
  } catch (error) {
    res.status(500).json({message: `Server error`});
  }
};
