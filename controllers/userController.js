const {Auth} = require('../models/authModel');
const bcrypt = require('bcrypt');

module.exports.profileInfo = async (req, res, next) => {
  const user = req.user;

  try {
    const foundUser = await Auth.findOne({_id: user._id});

    if (foundUser) {
      res.status(200).json({
        user: {
          _id: foundUser._id,
          username: foundUser.username,
          createdDate: foundUser.createdDate,
        },
      });
    } else {
      res.status(400).json({message: 'No user found'});
    }
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
};

module.exports.deleteProfileInfo = async (req, res, next) => {
  const user = req.user;
  try {
    const foundUser = await Auth.findOneAndRemove({_id: user._id});
    if (foundUser) {
      res.status(200).json({
        message: 'Success',
      });
    } else {
      res.status(400).json({message: 'No user found'});
    }
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
};

module.exports.updateProfilePassword = async (req, res, next) => {
  const user = req.user;
  const {oldPassword, newPassword} = req.body;
  const activeUser = await Auth.findOne({_id: user._id});
  try {
    if (await bcrypt.compare(oldPassword, activeUser.password)) {
      activeUser.password = await bcrypt.hash(newPassword, 10);
      await activeUser.save();
      return res.status(200).json({
        message: 'Success',
      });
    } else {
      return res.status(400).json({
        message: 'Failed',
      });
    }
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
};
