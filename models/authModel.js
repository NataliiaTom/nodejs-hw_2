const mongoose = require('mongoose');

// eslint-disable-next-line new-cap
const authSchema = mongoose.Schema({
  username: {
    require: true,
    type: String,
    unique: true,
  },
  password: {
    require: true,
    type: String,
    unique: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Auth = mongoose.model('Auth', authSchema);
