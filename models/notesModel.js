const mongoose = require('mongoose');

// eslint-disable-next-line new-cap
const noteSchema = mongoose.Schema({
  userId: {
    type: String,
    require: true,
  },
  completed: {
    type: Boolean,
    require: true,
  },
  text: {
    type: String,
    require: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});
module.exports.Note = mongoose.model('Note', noteSchema);
