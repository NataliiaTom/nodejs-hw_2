const express = require('express');
require('dotenv').config();
const app = express();
const mongoose = require('mongoose');
const morgan = require('morgan');

const userRouter = require('./router/userRouter');
const notesRouter = require('./router/notesRouter');
const authRouter = require('./router/authRouter');

app.use(express.json());
app.use('/api/users', userRouter);
app.use('/api/notes', notesRouter);
app.use('/api/auth', authRouter);

app.use(morgan('tiny'));

const port = process.env.PORT;

const getDBconnectionString = () =>
  `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOSTNAME}/${process.env.DB_NAME}`;

const start = async () => {
  await mongoose.connect(getDBconnectionString(), {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(port, () =>
    console.log(`Server has been started on port ${port}`),
  );
};
start();
